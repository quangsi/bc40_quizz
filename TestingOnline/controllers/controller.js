export let renderTitle = (currentIndex, questionList) => {
  document.getElementById('currentStep').innerText = `${currentIndex + 1}/${
    questionList.length
  } `;
};

export let checkSingleChoice = (answers) => {
  let id = document.querySelector('input[name="singleChoice"]:checked').value;
  // for (let i = 0; i < answers.length; i++) {
  //   if (answers[i].id == id) {
  //     return answers[i].exact;
  //   }
  let index = answers.findIndex((item) => {
    return item.id == id;
  });
  return answers[index].exact;
};
export let checkFillInput = () => {
  let inputEl = document.getElementById('fill-input');
  let userAnser = inputEl.value;
  let correctValue = inputEl.dataset.value;
  return userAnser == correctValue;
};
let renderSingleChoice = (question) => {
  let contentAnser = '';

  question.answers.forEach((item) => {
    let contentRadio = `<div class="form-check">
    <label class="form-check-label">
    <input type="radio" class="form-check-input" name="singleChoice" id="" value=${item.id} >
    ${item.content}
   </label>
   </div>`;
    contentAnser += contentRadio;
  });
  return contentAnser;
};

let renderFillInput = (content) => {
  return `<div class="form-group">
    <input type="text"

    data-value='${content}'
      class="form-control"  id="fill-input"  placeholder="Câu trả lời">
  
  </div>`;
};
export function renderQuestion(question) {
  let { questionType } = question;
  let contentQuestion = '';
  if (questionType == 1) {
    contentQuestion = renderSingleChoice(question);
  } else {
    contentQuestion = renderFillInput(question.answers[0].content);
  }

  document.getElementById('contentQuiz').innerHTML = `
  <h5>${question.content}</h5>
  ${contentQuestion}`;
}
