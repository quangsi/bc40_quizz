import { questionList as dataQuestion } from '../data/data.js';
// import dữ liệu questionList bằng tên mới là dataQuestion
import {
  checkFillInput,
  checkSingleChoice,
  renderQuestion,
  renderTitle,
} from './controller.js';

// tạo array mới từ dữ liệu ban đầu ( thêm key isCorrect )
let questionList = dataQuestion.map((item) => {
  return { ...item, isCorrect: null };
});

// currentIndex : index của câu hỏi hiện tại
let currentIndex = 0;

// render lần đầu

renderTitle(currentIndex, questionList);
renderQuestion(questionList[currentIndex]);
// khi user chọn câu tiếp theo
let nextQuestion = () => {
  // kiểm tra đáp án user chọn
  // questionList[currentIndex] :  câu hỏi hiện tại ( object )
  if (questionList[currentIndex].questionType == 1) {
    // validate
    if (document.querySelector('input[name="singleChoice"]:checked') == null) {
      return;
    }
    let isCorrect = checkSingleChoice(questionList[currentIndex].answers);
    questionList[currentIndex].isCorrect = isCorrect;
  } else {
    questionList[currentIndex].isCorrect = checkFillInput();
  }
  currentIndex++;
  if (currentIndex === questionList.length) {
    document.getElementById('startQuiz').style.display = 'none';
    document.getElementById('endQuiz').classList.remove('d-none');

    //  đếm số lượng câu hỏi đúng sai và show kết quả
  }
  renderTitle(currentIndex, questionList);
  renderQuestion(questionList[currentIndex]);
};
window.nextQuestion = nextQuestion;
